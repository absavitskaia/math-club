Соревнование от The French OR society (регистрацию пропустили)

https://www.roadef.org/challenge/2022/en/calendrier.php


Для студентов Lehigh University

https://coral.ise.lehigh.edu/~mopta/competition


DIMACS (регистрацию пропустили)

http://dimacs.rutgers.edu/programs/challenge/


The MiniZinc Challenge для задач (ОТКРЫТА до Fri, 2 June 2023)

https://www.minizinc.org/challenge.html


От информс (на паузе)

https://connect.informs.org/oratc/home


для студентов (открыто)

https://www.simio.com/academics/student-competition/


Прогнозирование

https://en.wikipedia.org/wiki/Makridakis_Competitions

https://mofc.unic.ac.cy/the-m6-competition/

Открыто про графы

https://pacechallenge.org/2023/

Оптимизация электросетей

https://gocompetition.energy.gov/challenges


Для студентов 

https://www.comap.com/contests


Поиск оптимальной траектории

https://sophia.estec.esa.int/gtoc_portal/?page_id=1099

задание прошлого года

https://sophia.estec.esa.int/gtoc_portal/wp-content/uploads/2021/12/gtoc11_problem_stmt.pdf


На интерес от IBM Research

https://research.ibm.com/haifa/ponderthis/challenges/March2023.html


на интерес от Online DM Community

https://dmcommunity.org/challenge/

Отсортировать

https://euro-neurips-vrp-2022.challenges.ortec.com/



