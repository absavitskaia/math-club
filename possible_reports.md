# Возможные темы докладов

## Теоретическая сессия

- **Ограничения "alldifferent" и "allequal" в МП**

материалы 

https://yetanothermathprogrammingconsultant.blogspot.com/2008/10/magic-squares-in-gams.html


- **Условие ИЛИ ИЛИ**

материалы

https://www.msi-jp.com/xpress/learning/square/10-mipformref.pdf Disjunctions + Minimum activity level


- **SOS переменные**

- **Частично целочисленные переменные**

материалы

https://www.msi-jp.com/xpress/learning/square/10-mipformref.pdf Partial integer variables

 - **Теневые цены в ЛП**

 - **Линеаризация нелинейных функций при помощи линейно кусочного представления**

материалы

 https://ampl.github.io/ampl-book.pdf с. 384

- **Open Source инструменты для IIS**

 материалы

 https://gekko.readthedocs.io/en/latest/global.html#coldstart

 https://github.com/pabloazurduy/python-mip-infeasibility

 http://www.sce.carleton.ca/faculty/chinneck/docs/GuieuChinneck.pdf

 https://pyomo.readthedocs.io/en/6.4.3/contributed_packages/iis.html

- **Автоматическая конвертация GAMS модели в LaTex. Утилита MODEL2TEX**

материалы 

https://www.gams.com/latest/docs/T_MODEL2TEX.html

- **Автоматическая конвертация MPS модели в GAMS модель**

материалы

https://www.gams.com/latest/docs/T_MPS2GMS.html

## Практическая сессия

- **Использование emdedded Python в моделях GAMS**

материалы

https://www.gams.com/archives/presentations/informs2017_EmbeddedCode.pdf

- **макросы в GAMS**

материалы

https://www.gams.com/42/docs/UG_DollarControlOptions.html#UG_DollarControl_MacrosInGAMS

- **Форматы MPS файлов**

материалы

https://lpsolve.sourceforge.net/5.5/mps-format.htm

https://en.wikipedia.org/wiki/MPS_(format)


 - **Warm Start в GAMS**

- **1334. Find the City With the Smallest Number of Neighbors at a Threshold Distance (Флойд Уоршелл)**

- **Неявное задание множеств в GAMS**

материалы

Implicit Set Definition (or: Domain Defining Symbol Declarations) в справке ГАМС
